## - RedePay SDK ##
----
This is a swift project for an iOS Framework that builds on RedePay's API to implementing its functionalities into other iOS applications.

This project uses Carthage for dependency management.
Be sure to get familiar with it before contributing.
Check dependencies below.

Install SwiftLINT before running the project.

### Technical Info ###

* Swift version: 3

#### Carthage dependencies: ####
* Alamofire ~> 4.4
* SwiftyJSON ~> HEAD
* ObjectMapper ~> 2.2


#### Other dependencies: ####
* SwiftLINT

## - How to Use ##
----

Before anything else, you want to set your API key in the project.  
Failing to do so will result in a fatal exception warning the developer he forgot to set an API Key.  
You can get an API Key from the RedePay control panel **once you are registered**.

__To configure your API Key, simply call__:
`RedePay.shared.config.apiKey = "[YOUR API KEY HERE]"`  

This must be set before using any other RedePay functionality, so it is recommended to set it in your `func application(didFinishLaunchingWithOptions:)` method.

Besides API Key, some functions depend on other configurations to be set.  
They are:  

* TokenNip (`RedePay.shared.config.tokenNIP`)
* PublicToken (`RedePay.shared.config.publicToken`)
* Order notification URL (`RedePay.shared.config.orderNotificationURL`)

Like with the API Key, if a method that needs one of these configurations is called before setting it, __the program will throw a fatal error to let the developer know__.

## - Methods ##
----

All RedePay methods **must be called on the singleton** `RedePay.shared`

* `placeOrder()`  parameters:
	- `reference: String`  
	__required!__ 
	_arbitrary string reference from vendor to identify the order in its own system_
	
	- `items: [RPItem]`  
	__required!__ 
	_array of valid_ `RPOrder` _objects to be ordered_
	
	- `discount: Int?`  
	_value of discount to be applied on the order (in cents)_  
	_ie: discount: 12346 equals R$ 123,46_
	
	- `settings: RPSettings?`  
	_general order settings, like number of installments for credit purchases and number of attempts in case of failure_
	
	- `customer: RPCustomer?`  
	_customer data like name and age_
	
	- `shipping: RPShipping?`  
	_shipping information, address and cost_
	
	- `url: [RPUrl]`  
	_array of urls used by the SDK's client._  
	_the can represent a callback for when the order is cancelled, a redirect URL for purchase completion, or a seller's notification callback_
	
	- __optional completion handler:__  
	_signature:_ `((_ order: RPOrder?, _ error: RPError?) -> Void)?`  
	_will return a `RPOrder` object in case of success, containing all of the order's details, or an `RPError` object, containing the server's error message response for easier debugging._  
	_both objects are mutually exclusive, if error is nil, the order is not and vice versa_
	
        __Code example:__
             	
            RedePay.shared.placeOrder("order_reference",
                                      items: [DummyParams.items],
                                      discount: 150,
                                      settings: DummyParams.settings,
                                      customer: DummyParams.customer,
                                      shipping: DummyParams.shipping,
                                      url: [DummyParams.redirectURL]) { order, error in
                                            
                if error != nil {
                    print(error!.description())
                    print(error!.errorDetails())
                    return
                }
                
                print(order!.orderID)
            }  


----


* `notifyOrder()` parameters: