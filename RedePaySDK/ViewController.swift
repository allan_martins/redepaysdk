//
//  ViewController.swift
//  RedePaySDK
//
//  Created by Allan Martins on 30/06/17.
//  Copyright © 2017 Allan Martins. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Configure RedePay
        RedePay.shared.config.apiKey = "7f6497a5-688a-43a1-be81-d0d43b0e2dc6"
        
        //Order item
        RedePay.shared.placeOrder("order_reference",
                                  items: [DummyParams.items],
                                  discount: 150,
                                  settings: DummyParams.settings,
                                  customer: DummyParams.customer,
                                  shipping: DummyParams.shipping,
                                  url: [DummyParams.redirectURL]) { order, error in
                                    
            if error != nil {
                print(error!.description())
                print(error!.errorDetails())
                return
            }
            
            RedePay.shared.detailsFor(orderID: order!.orderID,
                                      responseHandler: { orderDetails in
                                        if orderDetails != nil {
                                            print(orderDetails!.toJSONString(prettyPrint: true) ?? "failed")
                                            self.testRefund(order: orderDetails!.orderID,
                                                            amount: orderDetails!.amount!/3)
                                        }
                                        
            })
                                    
        }
        
    }
    
    func testRefund(order: String, amount: Int) {
        RedePay.shared
            .refund(amount, forOrder: order) { _, message, _, _ in
                print(message)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

struct DummyParams {
    
    static let items = RPItem(id: "item_id",
                                      description: "item_description",
                                      quantity: 1,
                                      price: 10000)
    
    static let recovery = RPShoppingCartRecovery()
    static let settings = RPSettings(installments: 1, totalAttempts: 1,
                                                 expirationDate: Date().addingTimeInterval(60*60*24*30),
                                                 cartRecovery: DummyParams.recovery)
    
    static let customer = RPCustomer(email: "allan.martins@gigigo.com.br",
                                                 name: "Allan Martins",
                                                 docs: [RPDocument(kind: "CPF",
                                                                   number: 39491792830)],
                                                 phones: [RPPhone(kind: "CELLPHONE",
                                                                  number: 11964338784)])
    
    static let address = RPAddress(street: "Rua Lobato",
                                   number: 145,
                                   postalCode: "03288010",
                                   district: "Vila Nova",
                                   city: "São Paulo", state: "SP",
                                   complement: "Casa 2")
    
    static let shipping = RPShipping(cost: 3500, address: DummyParams.address)
    
    static let redirectURL = RPUrl(kind: .redirect, url: "http://www.gigigo.com")
    
}
