//
//  RPRequest.swift
//  RedePaySDK
//
//  Created by Allan Martins on 03/07/17.
//  Copyright © 2017 Allan Martins. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import ObjectMapper

class RedePayRequest: Request {
    
    static let defaultHeaders: HTTPHeaders? = [
        "Content-Type": "application/json",
        "Accept": "application/json",
        "access-token": RedePay.shared.config.apiKey
    ]
    
    class func order(_ body: OrderRequestBody) -> DataRequest {
        
        let parameters: Parameters? = body.toJSON()
        
        return Alamofire.request(URL(string:"https://api.useredepay.com.br/orders")!,
                                 method: HTTPMethod.post,
                                 parameters: parameters,
                                 encoding: JSONEncoding.default,
                                 headers: RedePayRequest.defaultHeaders)
        
    }
    
    class func notifyOrder(_ id: String, reference: String, url: String? = nil,
                           amount: Float, nip: String, status: RPOrder.Status = .open)
    -> DataRequest {
        
        let parameters: [String: Any] = ["orderId": id,
                                         "reference": reference,
                                         "amount": amount,
                                         "token": nip,
                                         "status": status.rawValue]
        
        let callbackURL = url ?? RedePay.shared.config.orderNotificationURL
        
        return Alamofire.request(URL(string: callbackURL)!,
                                 method: HTTPMethod.post,
                                 parameters: parameters,
                                 encoding: URLEncoding.queryString,
                                 headers: RedePayRequest.defaultHeaders)
        
    }
    
    class func detailsForOrder(_ orderID: String) -> DataRequest {
        
        return Alamofire.request(URL(string: "https://api.useredepay.com.br/orders/\(orderID)")!,
                                 method: HTTPMethod.get,
                                 parameters: nil, encoding: URLEncoding.queryString,
                                 headers: RedePayRequest.defaultHeaders!)
        
    }
    
    class func orderRefund(transactionID: String, amount: Int) -> DataRequest {
        
        let urlString = "https://api.useredepay.com.br/transactions/\(transactionID)/refund"
        let params = RefundRequestBody(amount).toJSON()
        return Alamofire.request(URL(string: urlString)!,
                                 method: HTTPMethod.post,
                                 parameters: params, encoding: JSONEncoding.default,
                                 headers: RedePayRequest.defaultHeaders!)
        
    }
    
}

class RPRequestBody: Mappable {
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
    }
}

class RefundRequestBody: RPRequestBody {
    
    var amount: Int!
    
    required init?(map: Map) {
        super.init(map: map)
    }
    
    convenience init(_ refundAmount: Int) {
        self.init(JSON: ["amount": refundAmount])!
    }
    
    override func mapping(map: Map) {
        amount <- map["amount"]
    }
    
}

class OrderRequestBody: RPRequestBody {
    
    var reference: String!
    var discount: Int?
    var settings: RPSettings?
    var customer: RPCustomer?
    var shipping: RPShipping?
    var items: [RPItem]!
    var urls: [RPUrl]?
    
    required init?(map: Map) {
        super.init(map: map)
    }
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        
        reference   <- map["reference"]
        discount    <- map["discount"]
        settings    <- map["settings"]
        customer    <- map["customer"]
        shipping    <- map["shipping"]
        items       <- (map["items"], ListTransform())
        urls        <- (map["urls"], ListTransform())
    }
    
    convenience init(reference: String,
                     itemList: [[String: Any]],
                     discount: Int?,
                     settings: RPSettings?,
                     customer: RPCustomer?,
                     shipping: RPShipping?,
                     url: [RPUrl]?) {
        
        self.init(JSON: ["reference": reference,
                          "items": itemList])!
        self.discount = discount
        self.settings = settings
        self.customer = customer
        self.shipping = shipping
        self.urls = url
    }

}
