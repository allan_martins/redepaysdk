//
//  RedePayConfig.swift
//  RedePaySDK
//
//  Created by Allan Martins on 03/07/17.
//  Copyright © 2017 Allan Martins. All rights reserved.
//

import Foundation

struct RedePayConfig {
    
    private var _apiKey: String?
    public var apiKey: String {
        get {
            guard let key = _apiKey else {
                fatalError("API Key not set")
            }
            return key
        }
        set { _apiKey = newValue }
    }
    
    private var _tokenNIP: String?
    public var tokenNIP: String {
        get {
            guard let token = _tokenNIP else {
                fatalError("token nip not set")
            }
            return token
        }
        set { _tokenNIP = newValue }
    }
    
    private var _publicToken: String?
    public var publicToken: String {
        get {
            guard let token = _publicToken else {
                fatalError("public token not set")
            }
            return token
        }
        set { _publicToken = newValue }
    }
    
    private var _orderNotificationURL: String?
    public var orderNotificationURL: String {
        get {
            guard let url = _orderNotificationURL else {
                let p1 = "you must set a global order notification url in RedePayConfig "
                let p2 = "or provide one for custom callbacks"
                fatalError("\(p1)\(p2)")
            }
            return url
        }
        set { _orderNotificationURL = newValue }
    }
}
