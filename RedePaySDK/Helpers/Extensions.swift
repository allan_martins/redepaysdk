//
//  Extensions.swift
//  RedePaySDK
//
//  Created by Allan Martins on 03/07/17.
//  Copyright © 2017 Allan Martins. All rights reserved.
//

import Foundation
import Alamofire

extension Date {
    
    public static let redePayFormatter: DateFormatter = {
        let formatter = DateFormatter()
        let locale = Locale(identifier: "en_US_POSIX")
        formatter.locale = locale
        formatter.dateFormat = "YYYY-MM-dd'T'hh:mm:ssZZZZZ"
        return formatter
    }()
    
    func toRedePayString() -> String {
        return Date.redePayFormatter.string(from: self)
    }
    
}

extension String {
    
    func toRedePayDate() -> Date? {
        return Date.redePayFormatter.date(from: self)
    }
    
}

extension DataRequest {
    
    @discardableResult
    public func redePayResponse(
        line: Int = #line,
        file: String = #file,
        function: String = #function,
        queue: DispatchQueue? = nil,
        options: JSONSerialization.ReadingOptions = .allowFragments,
        breakOnError: Bool = false,
        completionHandler: @escaping (DataResponse<Any>, RPError?) -> Void)
        -> Self {
            
            return responseJSON(queue: queue, options: options) { dataResponse in
                
                var error = RPError.genericError("Erro desconhecido")
                let statusCode = dataResponse.response!.statusCode
                error.statusCode = statusCode

                if dataResponse.error != nil {
                    print(dataResponse.error!.localizedDescription)
                    print(error.errorDetails())
                    return
                }
                
                switch statusCode {
                case 200, 201, 202, 204:
                    //should validate format?
                    if dataResponse.result.value is [String: Any] ||
                    dataResponse.result.value is [Any] {
                        completionHandler(dataResponse, nil)
                    } else {
                        error.setErrorDetails(file: file, function: function, line: line)
                        if breakOnError { DataRequest.errorBreak(error) }
                        completionHandler(dataResponse, error)
                        return
                    }
                    break
                    
                case 400, 401, 403...405, 408, 413, 415, 422:
                    guard let response = dataResponse.result.value! as? [[String: Any]] else {
                        error.setErrorDetails(file: file, function: function, line: line)
                        if breakOnError { DataRequest.errorBreak(error) }
                        completionHandler(dataResponse, error)
                        return
                    }
                    
                    error = RPError(JSON: response[0])!
                    error.statusCode = statusCode
                    error.setErrorDetails(file: file, function: function, line: line)
                    if breakOnError { DataRequest.errorBreak(error) }
                    completionHandler(dataResponse, error)
                    break
                    
                case 500...599:
                    error.setErrorDetails(file: file, function: function, line: line)
                    error.code = ""
                    error.message = "Internal Server Error"
                    if breakOnError { DataRequest.errorBreak(error) }
                    completionHandler(dataResponse, error)
                    break
                    
                default:
                    error.setErrorDetails(file: file, function: function, line: line)
                    completionHandler(dataResponse, error)
                    if breakOnError { DataRequest.errorBreak(error) }
                    break
                }
                
            }
            
    }
    
    class func errorBreak(_ error: RPError, silent: Bool = false) {
        let lineFeed = "============================"
        let errorWarning = "FATAL SERVICE ERROR"
        let message =   "\n\n\(lineFeed)" +
                        "\n\(errorWarning)" +
                        "\n\nError description: \(error.description())" +
                        "\nError Details: \(error.errorDetails())" +
                        "\n\n\(lineFeed)\n"
        if silent {
            print(message)
            return
        }
        
        fatalError(message)
    }
    
}
