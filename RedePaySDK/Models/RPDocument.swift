//
//  RPDocument.swift
//  RedePaySDK
//
//  Created by Allan Martins on 04/07/17.
//  Copyright © 2017 Allan Martins. All rights reserved.
//

import Foundation
import ObjectMapper

class RPDocument: Mappable {
    var kind: String!
    var number: Int!
    
    init(kind: String, number: Int) {
        self.kind = kind
        self.number = number
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        kind    <- map["kind"]
        number  <- map["number"]
    }
}
