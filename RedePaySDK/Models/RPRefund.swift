//
//  RPRefund.swift
//  RedePaySDK
//
//  Created by Allan Martins on 12/07/17.
//  Copyright © 2017 Allan Martins. All rights reserved.
//

import Foundation
import ObjectMapper

class RPRefund: Mappable {
    
    var refundAmount: Int!
    var transactionAmount: Int!
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        refundAmount <- map["refundAmount"]
        transactionAmount <- map["transactionAmount"]
    }
    
}
