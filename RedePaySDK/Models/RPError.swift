//
//  RPError.swift
//  RedePaySDK
//
//  Created by Allan Martins on 07/07/17.
//  Copyright © 2017 Allan Martins. All rights reserved.
//

import Foundation
import ObjectMapper

public class RPError: Mappable {
    
    var statusCode: Int?
    var code: String?
    var message: String?
    var file: String?
    var function: String?
    var line: Int?
    
    required public init?(map: Map) {
        
    }
    
    public func mapping(map: Map) {
        self.code <- map["code"]
        self.message <- map["message"]
    }
    
    public class func genericError(_ message: String,
                                   code: String? = nil,
                                   statusCode: Int? = nil,
                                   line: Int = #line, function: String = #function, file: String = #file)
    -> RPError {
        let error = RPError(JSON: ["code": code ?? "", "message": message])
        error!.statusCode = statusCode
        error?.setErrorDetails(file: file, function: function, line: line)
        return error!
    }
    
    func setErrorDetails(file aFile: String, function aFunction: String, line aLine: Int) {
        file = aFile
        function = aFunction
        line = aLine
    }
    
    func errorDetails() -> String {

        guard let file = file, let function = function, let line = line else {
            return "No error details available"
        }
        
        do {
            let fileName = String(file.characters.split(separator: "/").last!)
            var nsFunctionName = NSMutableString(string: function)
            let regex = try NSRegularExpression(pattern: "\\w+?\\((.*?)\\)", options: [])
            let matchRange = NSRange(location: 0, length: function.characters.count)
            guard let match = regex.firstMatch(in: function, range: matchRange) else {
                nsFunctionName = NSMutableString(string: "UNKNOWN_FUNCTION")
                return "Line #\(line), \(fileName): \(nsFunctionName)"
            }
            
            nsFunctionName.replaceCharacters(in: match.rangeAt(1), with: "")
            return "Line #\(line), \(fileName): \(nsFunctionName)"
            
        } catch {
            print("regex error: \(error)")
        }
        
        return "No error details available"
        
    }
    
    func description() -> String {
        return "\(message ?? "Error message not set")(\(code ?? "nil")) statusCode: \(statusCode ?? -999)"
    }
    
}
