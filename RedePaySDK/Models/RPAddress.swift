//
//  RPAddress.swift
//  RedePaySDK
//
//  Created by Allan Martins on 04/07/17.
//  Copyright © 2017 Allan Martins. All rights reserved.
//

import Foundation
import ObjectMapper

class RPAddress: Mappable {
    var alias: String?
    var street: String!
    var number: Int!
    var complement: String?
    var postalCode: String!
    var district: String!
    var city: String!
    var state: String!
    
    init(street: String,
         number: Int,
         postalCode: String,
         district: String,
         city: String,
         state: String,
         alias: String? = nil,
         complement: String? = nil) {
        
        self.street = street
        self.number = number
        self.postalCode = postalCode
        self.district = district
        self.state = state
        self.city = city
        
        if complement != nil {
            self.complement = complement!
        }
        
        if alias != nil {
            self.alias = alias!
        }
        
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        alias       <- map["alias"]
        street      <- map["street"]
        number      <- map["number"]
        complement  <- map["complement"]
        postalCode  <- map["postalCode"]
        district    <- map["district"]
        city        <- map["city"]
        state       <- map["state"]
    }
}
