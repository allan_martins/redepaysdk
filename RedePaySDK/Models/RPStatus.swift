//
//  RPStatus.swift
//  RedePaySDK
//
//  Created by Allan Martins on 10/07/17.
//  Copyright © 2017 Allan Martins. All rights reserved.
//

import Foundation
import ObjectMapper

class RPStatus: Mappable {
    
    var reason: String?
    var responsible: String?
    var status: RPOrder.Status?
    
    required init?(map: Map) {
        
    }
    
    init(reason: String = "", responsible: String = "", status: RPOrder.Status = .open) {
        self.reason = reason
        self.responsible = responsible
        self.status = status
    }
    
    func mapping(map: Map) {
        reason <- map["reason"]
        responsible <- map["responsible"]
        status <- (map["status"], RPOrder.statusTransform)
    }
}
