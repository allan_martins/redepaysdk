//
//  RPShoppingCartRecovery.swift
//  RedePaySDK
//
//  Created by Allan Martins on 04/07/17.
//  Copyright © 2017 Allan Martins. All rights reserved.
//

import Foundation
import ObjectMapper

class RPShoppingCartRecovery: Mappable {
    var enable: Bool?
    var firstAlert: Int?
    var secondAlert: Int?
    var thirdAlert: Int?
    var fourthAlert: Int?
    var logoUrl: String?
    
    init() {
        enable = false
        firstAlert = 12
        secondAlert = 24
        thirdAlert = 48
        fourthAlert = 72
        logoUrl = "http://logoURL.com"
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        enable      <- map["enable"]
        firstAlert  <- map["firstAlert"]
        secondAlert <- map["secondAlert"]
        thirdAlert  <- map["thirdAlert"]
        fourthAlert <- map["fourthAlert"]
        logoUrl     <- map["logoUrl"]
    }
}
