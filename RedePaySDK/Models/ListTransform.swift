//
//  ListTransform.swift
//  RedePaySDK
//
//  Created by Allan Martins on 04/07/17.
//  Copyright © 2017 Allan Martins. All rights reserved.
//

import Foundation
import ObjectMapper

class ListTransform<T> : TransformType where T : Mappable {
    
    public typealias Object = [T]
    public typealias JSON = [[String: Any]]
    
    open func transformFromJSON(_ value: Any?) -> Object? {
        guard let safeValue = value as? JSON else {
            return nil
        }
        return safeValue.map { JSONValue -> T in
            return T(JSON: JSONValue)!
        }
    }
    
    open func transformToJSON(_ value: Object?) -> JSON? {
        if value == nil {
            return nil
        }
        return value!.map({ item -> [String: Any] in
            return item.toJSON()
        })
    }
    
    public init() {
        
    }
}
