//
//  RPItem.swift
//  RedePaySDK
//
//  Created by Allan Martins on 04/07/17.
//  Copyright © 2017 Allan Martins. All rights reserved.
//

import Foundation
import ObjectMapper

class RPItem: Mappable {
    var id: String!
    var description: String!
    var quantity: Int!
    var amount: Int!
    var discount: Int?
    var freight: Int?
    
    convenience init(id: String, description: String, quantity: Int, price: Int) {
        
        self.init(JSON: ["id": id,
                         "description": description,
                         "quantity": quantity,
                         "amount": price])!
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id          <- map["id"]
        description <- map["description"]
        quantity    <- map["quantity"]
        amount      <- map["amount"]
        discount    <- map["discount"]
        freight     <- map["freight"]
    }
}
