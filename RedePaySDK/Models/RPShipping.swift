//
//  RPShipping.swift
//  RedePaySDK
//
//  Created by Allan Martins on 04/07/17.
//  Copyright © 2017 Allan Martins. All rights reserved.
//

import Foundation
import ObjectMapper

class RPShipping: Mappable {
    var cost: Int?
    var address: RPAddress?
    
    init() {
        cost = 0
        address = nil
    }
    
    required init?(map: Map) {
        
    }
    
    convenience init(cost: Int, address: RPAddress) {
        self.init()
        self.cost = cost
        self.address = address
    }
    
    func mapping(map: Map) {
        cost    <- map["cost"]
        address <- map["address"]
    }

}
