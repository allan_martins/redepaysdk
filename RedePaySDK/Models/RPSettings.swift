//
//  RPSettings.swift
//  RedePaySDK
//
//  Created by Allan Martins on 04/07/17.
//  Copyright © 2017 Allan Martins. All rights reserved.
//

import Foundation
import ObjectMapper

class RPSettings: Mappable {
    var attempts: Int?
    var expiresAt: Date?
    var maxInstallments: Int!
    var shoppingCartRecovery: RPShoppingCartRecovery?
    
    init(max installments: Int) {
        maxInstallments = installments
        attempts = 1
        expiresAt = Date().addingTimeInterval(60*60*24*30)
        shoppingCartRecovery = RPShoppingCartRecovery()
    }
    
    init(installments: Int,
         totalAttempts: Int,
         expirationDate: Date,
         cartRecovery: RPShoppingCartRecovery?) {
        maxInstallments = installments
        attempts = totalAttempts
        expiresAt = expirationDate
        shoppingCartRecovery = cartRecovery
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
        attempts                <- map["attempts"]
        expiresAt               <- (map["expiresAt"], RPSettings.dateTransform)
        maxInstallments         <- map["maxInstallments"]
        shoppingCartRecovery    <- map["shoppingCartRecovery"]
        
    }
    
    public static let dateTransform = TransformOf<Date, String>(fromJSON: { dateString -> Date? in
        return dateString!.toRedePayDate()!
    }) { date -> String? in
        return date!.toRedePayString()
    }
    
}
