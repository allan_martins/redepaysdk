//
//  RPUrl.swift
//  RedePaySDK
//
//  Created by Allan Martins on 04/07/17.
//  Copyright © 2017 Allan Martins. All rights reserved.
//

import Foundation
import ObjectMapper

class RPUrl: Mappable {
    
    var kind: Kind!
    var url: String!
    
    enum Kind: String {
        
        //callBack de transações cancelada
        case cancel
        
        //para definir a URL para qual o comprador será direcionado 
        //após a conclusão do checkout
        case redirect
        
        //para notificações de alterações de estado nas transações
        case notification
        
        //para notificações de mudanças no estado do pedido
        case orderNotification
    }
    
    let kindTransform = TransformOf<Kind, String>(fromJSON: { stringValue -> RPUrl.Kind? in
        return Kind(rawValue: stringValue!)
    }) { kind -> String? in
        return kind!.rawValue
    }
    
    init(kind: Kind, url: String) {
        self.kind = kind
        self.url = url
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        kind    <- (map["kind"], kindTransform)
        url     <- map["url"]
    }
}
