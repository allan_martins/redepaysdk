//
//  RPCustomer.swift
//  RedePaySDK
//
//  Created by Allan Martins on 04/07/17.
//  Copyright © 2017 Allan Martins. All rights reserved.
//

import Foundation
import ObjectMapper

class RPCustomer: Mappable {
    var email: String!
    var name: String!
    var documents: [RPDocument]!
    var phones: [RPPhone]!
    
    init(email: String, name: String, docs: [RPDocument], phones: [RPPhone]) {
        self.email = email
        self.name = name
        self.documents = docs
        self.phones = phones
    }
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        email       <- map["email"]
        name        <- map["name"]
        documents   <- (map["documents"], ListTransform())
        phones      <- (map["phones"], ListTransform())
    }
}
