//
//  RPOrder.swift
//  RedePaySDK
//
//  Created by Allan Martins on 05/07/17.
//  Copyright © 2017 Allan Martins. All rights reserved.
//

import Foundation
import ObjectMapper

class RPOrder: Mappable {
    
    enum Status: String {
        case open = "OPEN"
        case cancelled = "CANCELLED"
        case completed = "COMPLETED"
        case dispute = "IN_DISPUTE"
        case reversed = "REVERSED"
        case chargeback = "CHARGEBACK"
    }

    var orderID: String!
    var reference: String!
    var amount: Int?
    var tokenNIP: String?
    var status: Status?
    
    var items: [RPItem]?
    var totalAmount: Int?
    var statusHistory: [RPStatus]?
    var shipping: RPShipping?
    var reversedAmount: Int?
    var reversalHistory: [RPReversal]? //TODO: find out what goes here and build a model
    var expireDate: Date?
    var discount: Int?
    var customer: RPCustomer?
    
    required init?(map: Map) {
        
    }
    
    init(orderID: String, reference: String) {
        self.orderID = orderID
        self.reference = reference
    }
    
    func mapping(map: Map) {
        orderID <- map["orderId"]
        reference <- map["reference"]
        amount <- map["amount"]
        tokenNIP <- map["token"]
        status <- (map["status"], RPOrder.statusTransform)
        items <- (map["items"], ListTransform())
        totalAmount <- map["totalAmont"]
        statusHistory <- (map["statusHistory"], ListTransform())
        shipping <- map["shipping"]
        reversedAmount <- map["reversedAmount"]
        reversalHistory <- (map["reversalHistory"], ListTransform())
        expireDate <- (map["expiresAt"], RPSettings.dateTransform)
        discount <- map["discount"]
        customer <- map["customer"]
    }
    
    // MARK: Transform
    
    public static let statusTransform = TransformOf<Status, String>(fromJSON: { stringValue -> RPOrder.Status? in
        return Status(rawValue: stringValue!.uppercased())
    }) { status -> String? in
        return status!.rawValue
    }
}
