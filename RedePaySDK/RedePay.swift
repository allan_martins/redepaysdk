//
//  RedePay.swift
//  RedePaySDK
//
//  Created by Allan Martins on 04/07/17.
//  Copyright © 2017 Allan Martins. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper

class RedePay: NSObject {
    
    static let shared = RedePay()
    var config = RedePayConfig()
    
    func placeOrder(_ reference: String,
                    items: [RPItem],
                    discount: Int? = nil,
                    settings: RPSettings? = nil,
                    customer: RPCustomer? = nil,
                    shipping: RPShipping? = nil,
                    url: [RPUrl]? = nil,
                    responseHandler: ((_ order: RPOrder?, _ error: RPError?) -> Void)? = nil) {
        
        let JSONItems = items.map { item -> [String: Any] in
            return item.toJSON()
        }
        
        let orderBody = OrderRequestBody(reference: reference,
                                         itemList: JSONItems,
                                         discount: discount,
                                         settings: settings,
                                         customer: customer,
                                         shipping: shipping,
                                         url: url)

        RedePayRequest.order(orderBody)
        .redePayResponse(completionHandler: { (dataResponse, error) in
            if error != nil {
                // Handle error
                DataRequest.errorBreak(error!, silent: true)
                return
            }
            
            if responseHandler == nil {
                return
            }
            
            guard let JSON = dataResponse.result.value as? [String: String] else { return }
            let order = RPOrder(orderID: JSON["id"]!, reference: JSON["reference"]!)
            responseHandler!(order, nil)
        })
    }
    
    func notifyOrder(_ orderID: String,
                     reference: String,
                     amount: Float,
                     NIP: String,
                     status: String,
                     callbackURL: String,
                     responseHandler: ((_ dataResponse: DataResponse<Any>, _ error: RPError?) -> Void)? = nil) {
    
        guard let safeStatus = RPOrder.Status(rawValue: status) else {
                debugPrint("invalid order status")
                return
        }
        
        RedePayRequest.notifyOrder(orderID, reference: reference, amount: amount,
                                   nip: RedePay.shared.config.orderNotificationURL,
                                   status: safeStatus)
        .redePayResponse(completionHandler: { dataResponse, error in
            if error != nil || responseHandler == nil {
                // Handle error
                return
            }
            print(dataResponse)
        })
        
    }
    
    func detailsFor(orderID: String,
                    responseHandler: @escaping (_ order: RPOrder?) -> Void) {
        
        RedePayRequest.detailsForOrder(orderID)
        .redePayResponse(completionHandler: { dataResponse, error in
            if error != nil {
                // Handle error
                return
            }
            
            guard let order = Mapper<RPOrder>().map(JSONObject: dataResponse.result.value!)
                else {
                    responseHandler(nil)
                    let error = RPError.genericError("failed to instantiate RPOrder object")
                    print(error.description())
                    print(error.errorDetails())
                    return
            }
            
            responseHandler(order)
        })
    }
    
    typealias RefundHandler =
    (_ success: Bool, _ message: String, _ refundedAmount: Int?, _ residualAmount: Int?) -> Void
    
    func refund(_ amount: Int, forOrder orderID: String,
                handler: @escaping RefundHandler) {
        
        RedePayRequest.orderRefund(transactionID: orderID, amount: amount)
        .redePayResponse { response, error in
            
            if error != nil {
                
                guard let message = error!.message else {
                    print(error!.errorDetails())
                    handler(false, "Erro desconhecido", nil, nil)
                    return
                }
                
                handler(false, message, nil, nil)
                return
            }
            
            guard let refundResponse = Mapper<RPRefund>().map(JSONObject: response.result.value!)
                else {
                    handler(false, "Falha no parse do retorno da API", nil, nil)
                    let error = RPError.genericError("failed to instantiate RPRefund object")
                    print(error.description())
                    print(error.errorDetails())
                    return
            }
            
            let message = "Valor estornado: \(refundResponse.refundAmount)"
            handler(true, message, refundResponse.refundAmount, refundResponse.transactionAmount)
            
        }
    }
}
